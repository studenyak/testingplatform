
# Help variables
RM := rm -rf
PROJ_NAME := KwebblTester
PROJ_PATH := /home/alex/Kwebbl/${PROJ_NAME}
BUILD_DIR := build
SRC_DIR := src
OBJ_DIR := ${BUILD_DIR}/obj
BIN_DIR := ${BUILD_DIR}/bin

# Object files to build release version
OBJS += \
	${OBJ_DIR}/main.o\
	${OBJ_DIR}/Config.o\
	${OBJ_DIR}/actor.o\
	${OBJ_DIR}/datagenerator.o\
	${OBJ_DIR}/useragent.o\
	${OBJ_DIR}/uacontroller.o\
	${OBJ_DIR}/registrationtest.o\
	${OBJ_DIR}/basedocument.o\
	${OBJ_DIR}/companydocument.o\
	${OBJ_DIR}/devicedocument.o\
	${OBJ_DIR}/extensiondocument.o\
	${OBJ_DIR}/userdocument.o\
	${OBJ_DIR}/useragentdocuments.o\
	${OBJ_DIR}/ConnectionPool.o\
	${OBJ_DIR}/callingtest.o\
	${OBJ_DIR}/documentmanager.o\
	${OBJ_DIR}/useragentmanager.o\
	${OBJ_DIR}/devicecontroller.o\
	${OBJ_DIR}/documentcontroller.o\
	${OBJ_DIR}/statisticscollector.o

# Include paths for building release version
INC_PATHS += \
	-Isrc/SimpleAmqpClient\
	-I../FMS/src

# Librarys for building release version
LIBS := -lSimpleAmqpClient -lrabbitmq -lboost_thread -lboost_system -ldl -lCouchDB

# Library paths for building release version
LIB_PATHS += \
	-Llib

DEFS := -DBOOST_SPIRIT_THREADSAFE -DFIRST_START

# Rules for building object files for release
${OBJ_DIR}/%.o: ${SRC_DIR}/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++  ${DEFS} ${INC_PATHS} -O0 -g3 -Wall -c -fmessage-length=0 -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

# Rules for building object files for release
${OBJ_DIR}/%.o: ${SRC_DIR}/Beans/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++  ${DEFS} ${INC_PATHS} -O0 -g3 -Wall -c -fmessage-length=0 -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

# Rules for building object files for release
${OBJ_DIR}/%.o: ${SRC_DIR}/Mediators/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++  ${DEFS} ${INC_PATHS} -O0 -g3 -Wall -c -fmessage-length=0 -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

# Rules for building object files for release
${OBJ_DIR}/%.o: ${SRC_DIR}/Helpers/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++  ${DEFS} ${INC_PATHS} -O0 -g3 -Wall -c -fmessage-length=0 -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

# Rules for building object files for release
${OBJ_DIR}/%.o: ${SRC_DIR}/Controllers/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++  ${DEFS} ${INC_PATHS} -O0 -g3 -Wall -c -fmessage-length=0 -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

# Rules for building object files for release
${OBJ_DIR}/%.o: ${SRC_DIR}/RegistrationTest/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++  ${DEFS} ${INC_PATHS} -O0 -g3 -Wall -c -fmessage-length=0 -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

# Build release and test versions
all: init $(OBJS)
	@echo 'Building target: $@'
	@echo 'Invoking: GCC C++ Linker'
	g++ ${LIB_PATHS} -o ${BIN_DIR}/${PROJ_NAME} $(OBJS) $(LIBS)
	patchelf --set-rpath ${PROJ_PATH}/lib ${BIN_DIR}/${PROJ_NAME}
#	install_name_tool -add_rpath ${PROJ_PATH}/lib ${BIN_DIR}/${PROJ_NAME}
	@echo 'Finished building target: ${PROJ_NAME}'
	@echo ' '

init:
	mkdir -p ${BUILD_DIR}
	mkdir -p ${BIN_DIR}
	mkdir -p ${OBJ_DIR}

# Clean all builded versions and object files
clean:
	-$(RM) ${BUILD_DIR}

