/*
 * IDatabase.h
 *
 *  Created on: May 27, 2013
 *      Author: Oleksandr Studenyak
 */

#ifndef IDATABASE_H_
#define IDATABASE_H_

//#include "common.h"
#include <vector>
#include <set>
#include <map>
#include <string>
#include <boost/any.hpp>
//#include "../PluginBase/src/IPlugin.h"

//! \brief Describes data that is stored in document
typedef std::vector<boost::any> arrayOfDocData;

//! \brief Describes map of documents
typedef std::map<std::string, arrayOfDocData> mapOfDocuments; //!< Key - name of document where data is saved
                                                              //!< Value - array of data

//! \brief Describes types of variables that are stored in database
//! \note By implementing of IDatabase user should consider that not
//!       all databases supports this enum of types.
typedef enum {
	DB_INT,        /**< represents an 32 bit integer number      */
	DB_BIGINT,     /**< represents an 64 bit integer number      */
	DB_DOUBLE,     /**< represents a floating point number       */
	DB_STRING,     /**< represents a zero terminated const char* */
	DB_STR,        /**< represents a string of 'str' type        */
	DB_DATETIME,   /**< represents date and time                 */
	DB_BLOB,       /**< represents a large binary object         */
	DB_BITMAP      /**< an one-dimensional array of 32 flags     */
} typeOfDbVariable;

//!\ brief Describes map of variables that user wants to get from database
typedef std::map<std::string, typeOfDbVariable> mapOfDataDescriptions; //!< Key - name of variable in datbase (dbData)
                                                                  //!< Value - type of variable

//! \brief Describes operands
//!
//! Temporary operands are strings.
class pairOfOperands : public std::pair<std::string, std::string>
{
public:
	pairOfOperands(std::string f, std::string s) : std::pair<std::string, std::string>(f, s)
	{
	}

	operator std::pair<std::string, std::string>()
	{
		return *this;
	}
};

//template <class T>
//class myPair : public std::pair<std::string, T>
//{
//public:
//	myPair(std::string f, T s) : std::pair<std::string, T>(f, s)
//	{}
//
//	operator std::pair<std::string, T>()
//	{
//		return *this;
//	}
//};


//typedef std::pair<std::string, std::string> pairOfOperands; //!< first - left operand
                                                            //!< second - right operand

//! \brief Describes set of conditions to build view
//!
//! Conditions will interpret as pairOfOperands[0].first=pairOfOperands[0].second &&
//!                              pairOfOperands[1].first=pairOfOperands[1].second &&
//!                              ...
//!                              pairOfOperands[n].first=pairOfOperands[n].second &&
typedef std::set<pairOfOperands> setOfViewConditions; //!< Key - left operand of "=" condition
                                                      //!< Value - right operand of "=" condition

//! \brief An eOperation enum type. Describes operation for selecting data
//!
//! \note By implementing of IDatabase user should consider that not
//!       all databases supports this enum of operations.
typedef enum
{
	LessThan = 0,//!< a < b
	GreaterThan, //!< a > b
	Equal,		 //!< a == b
	BitwiseAnd,  //!< a in [x, y]
	SetOfKeys,	 //!< a is one of [x..y]
    AllData      //!< all data from view
} typeOfOperation;

//! \brief Describes simple operation with data
//!
//! This will be interpeted as pairOfOperands.first typeOfOperation pairOfOperands.second
//! For example if pairOfOperands = "x", 2; and typeOfOperation = eOperationLessThan
//! describes following condition x < 2
typedef std::pair<pairOfOperands, typeOfOperation> dataCondition;

//! \brief Describes set of conditions
//!
//!  This will be interpeted as dataCondition[0] && dataCondition[1] && ... dataCondition[n]
typedef std::set<dataCondition> setOfDataConditions;


//! \brief Structure that describes loaded plugin
typedef struct
{
    void* m_pHandle; //!< Handle to the loaded library where plugin is implemented
    std::string m_strName; //!< Name of the plugin
    std::string m_strPath;//!< Path to the plugin library
//    IPlugin* m_pPlugin;//!< Interface to loaded plugin
} typePLUGIN_DATA;


//! \brief Interface to database
class IDatabase
{
public:

	//! \brief Gets value from table without metadata
	//! \param[in] strKey Key of the value
	//! \param[in] strId ID of the document where to search
	//! \return Result value for this request
	virtual boost::any queryValue(const std::string& strKey,
	                              const std::string& strId) = 0;

	//! \brief Gets value from table with metadata
	//! \param[in] strKey Key of the value
	//! \param[in] strTableName Name of the table according to metadata
	//! \param[in] strId ID of the document where to search
	//! \return Result value for this request
	virtual boost::any queryValue(const std::string& strKey,
	                              const std::string& strTableName,
	                              const std::string& strId) = 0;

	//! \brief Writes data to database
	//! \param[in] strKey Key of the value
	//! \param[in] anyValue Value that should be written/rewritten to database
	//! \param[in] strId ID of the document where to write
	//! \return true - if operation is successful, and false - otherweis
	virtual bool        writeValue(const std::string& strKey,
	                               const boost::any& anyValue,
	                               const std::string& strId) = 0;

	//! \brief Shows if connection to database is available
	//! \return true - if connection to database is available, and false - if not
	virtual bool        isConnected(void) = 0;

	//! \brief Gets data for plugins that should be loaded
	//! \param[in,out] vPluginTable Vector of typePLUGIN_DATA structures
	//! \return Count of plugins that are available for loading
	virtual int         queryPlugins(std::vector<typePLUGIN_DATA>& vPluginTable) = 0;

	//! \brief Query data by defined conditions
	//!
	//! \param dataToGetFromDB Data structure that user wants to get from database
	//! \param setOfViewConditions Set of conditions to select data for building view
	//! \param setOfDataConditions Set of conditions to select data from builded view
	//! \return Map of documents with data that matchs setted conditions
	virtual mapOfDocuments queryDataByParams(const mapOfDataDescriptions& dataToGetFromDB,
			                                 const setOfViewConditions& ConditionForBuildingView,
			                                 const setOfDataConditions& setOfDataParams) = 0;

	//! \brief Creates privileges
	//!
	//! \param anyDocData Data structure that user wants to write to privileges
	//! \return Identifyer of new created document.
	virtual std::string       createDocument(const boost::any& anyDocData) = 0;

    //! \brief Removes document from database
    //! \param[in] strId ID of the document where to search
    //! \return true - if operation is successful, and false - otherweis
    virtual bool removeDocumentById(const std::string& strId) = 0;

    virtual void release() = 0;
};

#endif /* IDATABASE_H_ */
