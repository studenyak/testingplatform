#ifndef REGISTRATIONTEST_H
#define REGISTRATIONTEST_H

#include "../Controllers/devicecontroller.h"
#include "statisticscollector.h"
class CRegistrationTest
{
public:
    CRegistrationTest();
    ~CRegistrationTest();
    unsigned int start(void);
    void stop(void);
private:
    CDeviceController m_Devices;
    CStatisticsCollector* m_pStat;
    CConfig* m_pConf;
};

#endif // REGISTRATIONTEST_H
