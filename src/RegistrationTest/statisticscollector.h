#ifndef STATISTICSCOLLECTOR_H
#define STATISTICSCOLLECTOR_H

#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <sys/time.h>
#include "../Helpers/Config.h"

class CStatisticsCollector
{
public:

    ~CStatisticsCollector();
    //! \brief Method that creates and returns singleton object
    static CStatisticsCollector& getInstance()
    {
        static CStatisticsCollector stat;
        return stat;
    }

    std::ofstream m_csvFile;
    timeval getCurTime(void);
    float diffTime_msec(timeval& start_time, timeval& end_time);
    void write(void);
    void setConnectionTime(float t){m_fConnectionTime_ms = t;}
    void setCreationTime(float t){m_fCreationTime_ms = t;}

private:
    CStatisticsCollector();

    float m_fConnectionTime_ms;
    float m_fCreationTime_ms;
};

#endif // STATISTICSCOLLECTOR_H
