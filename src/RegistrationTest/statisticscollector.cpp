#include "statisticscollector.h"

CStatisticsCollector::CStatisticsCollector()
    : m_csvFile(CConfig::getInstance().getStatisticFileName().c_str())
{
    m_csvFile << "Date_sec;connTime_ms;createTime_ms" << std::endl;
    m_fConnectionTime_ms = 0.0;
    m_fCreationTime_ms = 0.0;
}

CStatisticsCollector::~CStatisticsCollector()
{
    m_csvFile.close();
}

timeval CStatisticsCollector::getCurTime()
{
    timeval time;
    gettimeofday(&time, NULL);
    return time;
}

float CStatisticsCollector::diffTime_msec(timeval& start_time, timeval& end_time)
{
    float diffTime = (end_time.tv_sec - start_time.tv_sec)*1000 + (float)(end_time.tv_usec - start_time.tv_usec)/1000;
    return diffTime;
}

void CStatisticsCollector::write(void)
{
    m_csvFile << getCurTime().tv_sec << ";"
              << m_fConnectionTime_ms << ";"
              << m_fCreationTime_ms << std::endl;

    m_fConnectionTime_ms = 0.0;
    m_fCreationTime_ms = 0.0;
}
