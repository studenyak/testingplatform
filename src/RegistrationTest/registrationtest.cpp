#include "registrationtest.h"
#include <sys/types.h>
#include <sys/wait.h>

CRegistrationTest::CRegistrationTest()
{
    m_pConf = &CConfig::getInstance("config.ini");
    m_pStat = &CStatisticsCollector::getInstance();
}

CRegistrationTest::~CRegistrationTest()
{
}

unsigned int CRegistrationTest::start(void)
{
    int nDevices = m_Devices.createDevices(CConfig::getInstance().getDeviceCount());
    if(nDevices)
    {
        printf("Run sipp and wait until it will end testing.\n");
        // Run sipp
        char chCommandLine[4096]={0};
        CConfig* pConf = &CConfig::getInstance();
        sprintf(chCommandLine, "%s %s -sf %s -inf %s -i %s -m %d -l %d -r %d -trace_rtt -trace_screen"
                , pConf->getRegTestCommand().c_str()
                , pConf->getSipServer().c_str()
                , pConf->getSippXml().c_str()
                , pConf->getSippCsv().c_str()
                , pConf->getLocalIP().c_str()
                , nDevices
                , pConf->getCountOfCallsPerSec()
                , nDevices);
        printf("%s\n",chCommandLine);
        system(chCommandLine);
    }
    //m_Devices.removeDevices();
    return 0;
}

void CRegistrationTest::stop(void)
{
    printf("Registration test is ready for stop. Press q or type quit.\n");
    std::string strMsg;
    do
    {
        char str[256] = {0};
        gets(str);
        strMsg.assign(str);
    }
    while(strMsg.compare("quit") && strMsg.compare("q") && strMsg.compare("exit"));
    m_Devices.removeDevices();
}
