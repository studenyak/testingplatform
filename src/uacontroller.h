#ifndef UACONTROLLER_H
#define UACONTROLLER_H

#include <map>
#include <string>
#include "useragent.h"

class CUAController
{
public:
    CUAController();
    virtual ~CUAController();

    void runUAs();
    void stopRegisteringUAs();
    void cleanUp();
    CUserAgent* selectUAS();
    CUserAgent* chooseUAC();

private:
    std::string generateNewPhoneNumber(int nDelta);
    std::map<std::string, CUserAgent*> m_mapOfUserAgents;
    std::map<std::string, CUserAgent*> m_mapOfUACs;
    std::map<std::string, CUserAgent*> m_mapOfUASs;

    void makeSimpleCall();
};

#endif // UACONTROLLER_H
