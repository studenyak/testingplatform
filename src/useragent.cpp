#include "useragent.h"
#include "Helpers/Config.h"
#include "datagenerator.h"
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include "uacontroller.h"

CUserAgent::CUserAgent(std::string strPhoneNumber, CUAController *pUaController) :
    m_nStatus(idle)
    , m_pUaController(pUaController)
      , m_strPhoneNumber(strPhoneNumber)
      , m_bRun(true)
{
    CConfig config = CConfig::getInstance("rabbitMq.ini");
    m_nRegistrationPeriod = config.getUARegistrationPeriod();
    m_nType = (typeOfUA)(rand()%2);
}

CUserAgent::~CUserAgent()
{
    stop();
}

void CUserAgent::start()
{
    m_RegisteringThread = boost::thread(CUserAgent::registration_proc, this);
}

void CUserAgent::stop()
{
    m_bRun = false;
    m_Condition.notify_one();
    m_RegisteringThread.join();
}

void CUserAgent::registration_proc(void *pArg)
{
    CUserAgent* pCtx = (CUserAgent*)(pArg);
    if(!pCtx)
    {
        printf("ERROR: Null context.\n");
        return;
    }

    // Sleep random seconds between 0 ,,, 5
    sleep(rand() % 5);
    printf("INFO: User Agent %s runs registration process. Registration period: %d\n"
           , pCtx->m_strPhoneNumber.c_str()
           , pCtx->m_nRegistrationPeriod);

    if(!pCtx->m_nRegistrationPeriod)
    {
        printf("ERROR: Invalid registration period.\n");
        return;
    }
    pCtx->reg();

    while(pCtx->m_bRun)
    {
        // Registration proces have just to run sipp
        // with neccessary arguments in setted rate

        // NOTE: Registratino period is temoprary hard coded to 60 sec
        // TODO: Change it

        // Wait for your time
        boost::system_time const timeout = boost::get_system_time() + boost::posix_time::seconds(pCtx->m_nRegistrationPeriod);
        boost::unique_lock<boost::mutex> lock(pCtx->m_Lock);
        if(pCtx->m_Condition.timed_wait(lock,timeout) == false)
        {
            printf("INFO: REGISTER of UserAgent %s\n", pCtx->m_strPhoneNumber.c_str());

            // Register process
            pCtx->reg();
        }
        else
            pCtx->m_bRun = false;
    }
    printf("User Agent %s stoped registration process.\n", pCtx->m_strPhoneNumber.c_str());
}

void CUserAgent::calling_proc(void *pArg)
{
//    char* strSippCmd = (char*)pArg;
//    system(strSippCmd);
//    m_nStatus = idle;
}

void CUserAgent::reg()
{
    // Generate new xml file if it does not exist yet

    CDataGenerator dg;
    std::string strXmlFile = dg.generateREGISTERxml(this);
    if(strXmlFile.empty())
    {
        printf("ERROR: Path to xml file is empty.\n");
        return;
    }

    // Run sipp
    std::string strSippCmd = "./sipp 192.168.150.90 -sf ";
    strSippCmd += strXmlFile;
    strSippCmd += " -m 1 -bg -t un -l 300";
    printf("INFO: REGISTER for user agent %s\n", m_strPhoneNumber.c_str());
    system(strSippCmd.c_str());
}

void CUserAgent::initiateCall(CUserAgent *pUAS)
{
//    CDataGenerator dg;
//    std::string strXmlFile = dg.generateINVITExml(this, pUAS);

//    // Run sipp
//    std::string strSippCmd = "./sipp 192.168.150.90 -sf ";
//    strSippCmd += strXmlFile;
//    strSippCmd += " -m 1 -bg -t un -l 300";
//    printf("INFO: User %s calls to %s\n", m_strPhoneNumber.c_str(), pUAS->getPhoneNumber().c_str());

//    if(m_nStatus == idle)
//    {
//        m_nStatus = calling;
//        m_CallingThread  = boost::thread(CUserAgent::calling_proc, strSippCmd.c_str());
//    }
//    else
//        printf("INFO: User agent is already calling. Please call later.\n");
}

void CUserAgent::receiveCall()
{
//    // Run sipp
//    std::string strXmlFile = "RecvCall.xml";
//    std::string strSippCmd = "./sipp 192.168.150.90 -sf ";
//    strSippCmd += strXmlFile;
//    strSippCmd += " -m 1 -bg -t un -l 300";
//    printf("INFO: User %s waits for the call\n", m_strPhoneNumber.c_str(), pUAS->getPhoneNumber().c_str());

//    if(m_nStatus == idle)
//    {
//        m_nStatus = calling;
//        m_CallingThread  = boost::thread(CUserAgent::calling_proc, strSippCmd.c_str());
//    }
//    else
//        printf("INFO: User agent is already calling. Please call later.\n");
}


std::string CUserAgent::getSipServerAddr()const
{
    return m_strSipServerAddr;
}

std::string CUserAgent::getPhoneNumber() const
{
    return m_strPhoneNumber;
}

int CUserAgent::getRegistrationPeriod() const
{
    return m_nRegistrationPeriod;
}
