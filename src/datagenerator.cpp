#include "datagenerator.h"
#include <stdlib.h>
#include <stdio.h>
#include "useragent.h"
#include <fstream>

CDataGenerator::CDataGenerator()
{
}

int CDataGenerator::generateTimeDelay(void)
{
    return rand() % 200; // 0...200
}

int CDataGenerator::generateCallDuration(void)
{
    return rand() % 600; // 0...600
}

std::string CDataGenerator::generatePhoneNumber(void)
{
    std::string str;
    int nDigitsCount = rand() % 4 + 12;
    for(int i = 0; i < nDigitsCount; i++)
    {
        if(i == 2)
            str.append("0");
        else
        {
            char ch[10] = {0};
            sprintf(ch, "%d", rand() % 10);
            str.append(ch);
        }
    }
    return str;
}

std::string CDataGenerator::generateREGISTERxml(CUserAgent* pUA)
{
    // Check if needed file is already exist
    std::string strXmlFileName = "xml/REGISTER_";
    strXmlFileName += pUA->getPhoneNumber();
    strXmlFileName += ".xml";

    FILE* pFile;
    pFile = fopen(strXmlFileName.c_str(), "r");
    if(pFile)
    {
       fclose(pFile);
       return strXmlFileName;
    }

    // If needed fiel does not exist than create one
    std::string strXmlTemplateFileName("REGISTER_tmplt.txt");

    // Get xml template file for REGISTER
    std::fstream fsXmlRegTemplate;

    fsXmlRegTemplate.open(strXmlTemplateFileName.c_str(), std::fstream::in);
    if(!fsXmlRegTemplate)
        return "";

    // Get length of template file:
    fsXmlRegTemplate.seekg (0, fsXmlRegTemplate.end);
    int nLength = fsXmlRegTemplate.tellg();
    fsXmlRegTemplate.seekg (0, fsXmlRegTemplate.beg);

    // Read template file
    char *bufContent = new char[nLength];
    fsXmlRegTemplate.read (bufContent,nLength);
    fsXmlRegTemplate.close();

    // Create new file with neccessary variables
    FILE* pFileXmlNew = fopen(strXmlFileName.c_str(), "w");
    if(!pFileXmlNew)
    {
        printf("WARNING: Can not create file %s", strXmlFileName.c_str());
        delete[] bufContent;
        return "";
    }

    if(fprintf(pFileXmlNew, bufContent, pUA->getPhoneNumber().c_str()
                                    , pUA->getRegistrationPeriod()) < 0)
    {
        fclose(pFileXmlNew);
        delete[] bufContent;
        return "";
    }

    fclose(pFileXmlNew);
    delete[] bufContent;

    return strXmlFileName;
}

std::string CDataGenerator::generateINVITExml(CUserAgent* pCaller, CUserAgent* pCallee)
{
    if(!pCaller)
    {
        printf("ERROR: Null pointer of caller.");
        return "";
    }
    if(!pCallee)
    {
        printf("ERROR: Null pointer of callee.");
        return "";
    }
    // Check if needed file is already exist
    std::string strXmlFileName = "xml/INVITE_";
    strXmlFileName += pCaller->getPhoneNumber();
    strXmlFileName += "_to_";
    strXmlFileName += pCallee->getPhoneNumber();
    strXmlFileName += ".xml";

    // Create file
    std::string strXmlTemplateFileName("INVITE_tmplt.txt");

    // Get xml template file for INVITE
    std::fstream fsXmlInviteTemplate;

    fsXmlInviteTemplate.open(strXmlTemplateFileName.c_str(), std::fstream::in);
    if(!fsXmlInviteTemplate)
        return "";

    // Get length of template file:
    fsXmlInviteTemplate.seekg (0, fsXmlInviteTemplate.end);
    int nLength = fsXmlInviteTemplate.tellg();
    fsXmlInviteTemplate.seekg (0, fsXmlInviteTemplate.beg);

    // Read template file
    char *bufContent = new char[nLength];
    fsXmlInviteTemplate.read (bufContent,nLength);
    fsXmlInviteTemplate.close();

    // Create new file with neccessary variables
    FILE* pFileXmlNew = fopen(strXmlFileName.c_str(), "w");
    if(!pFileXmlNew)
    {
        printf("WARNING: Can not create file %s", strXmlFileName.c_str());
        delete[] bufContent;
        return "";
    }

    if(fprintf(pFileXmlNew, bufContent, pCaller->getPhoneNumber().c_str()
                                    , pCallee->getPhoneNumber().c_str()) < 0)
    {
        fclose(pFileXmlNew);
        delete[] bufContent;
        return "";
    }

    fclose(pFileXmlNew);
    delete[] bufContent;

    return strXmlFileName;
}
