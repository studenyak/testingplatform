#include "Config.h"
#include <iostream>
#include <stdio.h>
#include "actor.h"
#include "uacontroller.h"
#include "RegistrationTest/registrationtest.h"
#include "RegistrationTest/statisticscollector.h"
#include "Controllers/callingtest.h"
using namespace std;

//! brief This test simulates messages for RabbitMQ
//! It creates clients (actors) with own phids and
//! and own threads.
//! Note You have to start Rating Engine daemon to
//! get results.
void RatingEngineTest()
{
//    CActor* actor[5] = {new CActor("dnise0tvs19u"),
//                        new CActor("wiy21vtwja1e"),
//                        new CActor("r4stmjn9mmq6"),
//                        new CActor("11nwj5tkhvhd"),
//                        new CActor("psexrw2xawtf")};
    CActor* actor[5] = {new CActor("984333f86380ec975101b519c0a68a4c"),
                        new CActor("984333f86380ec975101b519c0a6bb16"),
                        new CActor("984333f86380ec975101b519c0a6e7c8"),
                        new CActor("984333f86380ec975101b519c0a713bb"),
                        new CActor("984333f86380ec975101b519c0a74f01")};
    std::string strMsg;
    do
    {
        char str[256] = {0};
        gets(str);
        strMsg.assign(str);
    }
    while(strMsg.compare("quit"));

    // Clean up
    for(int i = 0; i < 5; i++)
        delete actor[i];
}

void UACTest()
{
    CUAController controller;
    controller.runUAs();

    std::string strMsg;
    do
    {
        char str[256] = {0};
        gets(str);
        strMsg.assign(str);
    }
    while(strMsg.compare("quit"));

    controller.cleanUp();
}
int main(int argc, char *argv[])
{
    cout << "Start testing" << endl;
    CConfig::getInstance(argv[1]);
    //RatingEngineTest();
    //UACTest();

    //CRegistrationTest test;
    //test.start();
    //test.stop();

    CCallingTest calling;
    calling.start();

    return 0;
}

