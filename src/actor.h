#ifndef ACTOR_H
#define ACTOR_H

#include <boost/thread.hpp>
#include <boost/thread/condition_variable.hpp>
#include "SimpleAmqpClient/SimpleAmqpClient.h"
#include <string>

using namespace AmqpClient;

//! \brief  This class represents simple clent that throws
//!         json messages in MQ for Rating engine in its own thread.
//!         JSON message is generated from random data.
class CActor
{
public:
    CActor(const std::string& strPhid);
    ~CActor();
    void start();
    void stop();

private:
    std::string m_strPhid;
    int m_nCallDuration;
    std::string m_strPhoneNumber;

    boost::thread m_Thread;
    static void proc(void *pArg);
    boost::mutex m_Lock;
    boost::condition_variable m_Condition;

    Channel::ptr_t m_pConnection;

    void sendMsg(const std::string& strMsg);
    bool    m_bRun;

};

#endif // ACTOR_H
