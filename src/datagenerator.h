#ifndef DATAGENERATOR_H
#define DATAGENERATOR_H

#include <string>
class CUserAgent;
class CDataGenerator
{
public:
    CDataGenerator();
    int generateTimeDelay(void);

    //! \brief  Random generate call dureation in seconds
    //!         between 0 and 600;
    //! \return Duration in seconds.
    int generateCallDuration(void);

    std::string generatePhoneNumber(void);

    std::string generateREGISTERxml(CUserAgent* pUA);
    std::string generateINVITExml(CUserAgent* pCaller, CUserAgent* pCallee);
};

#endif // DATAGENERATOR_H
