#include "uacontroller.h"
#include "Helpers/Config.h"
#include <stdlib.h>
#include <stdio.h>

CUAController::CUAController()
{
}

CUAController::~CUAController()
{
}

void CUAController::runUAs()
{
    CConfig config = CConfig::getInstance("rabbitMq.ini");
    for(int i = 0; i < config.getUACount(); i++)
    {
        // Create UA with new phone number
        CUserAgent* pUA = new CUserAgent(generateNewPhoneNumber(i), this);
        m_mapOfUserAgents[pUA->getPhoneNumber()] = pUA;
        if(pUA->getType() == CUserAgent::client)
            m_mapOfUACs[pUA->getPhoneNumber()] = pUA;
        else
            m_mapOfUASs[pUA->getPhoneNumber()] = pUA;

        printf("DEBUG: User agent with number %s is created.\n", pUA->getPhoneNumber().c_str());
    }
    printf("INFO: Count of created UAC is %d.\n", (int)m_mapOfUACs.size());
    printf("INFO: Count of created UAS is %d.\n", (int)m_mapOfUASs.size());

    // Run user agents
    for(std::map<std::string, CUserAgent*>::iterator it = m_mapOfUserAgents.begin();
        it != m_mapOfUserAgents.end();
        ++it)
    {
        it->second->start();
    }

}

std::string CUAController::generateNewPhoneNumber(int nDelta)
{
    CConfig config = CConfig::getInstance("rabbitMq.ini");
    unsigned long int lStartPhoneNumber = atol(config.getUAStartPhoneNumber().c_str());
    unsigned long int lNewPhoneNumber = lStartPhoneNumber + nDelta;

    char strNewPhoneNumber[0xFF] = {0};
    sprintf(strNewPhoneNumber, "%ld", lNewPhoneNumber);

    return std::string(strNewPhoneNumber);
}

void CUAController::stopRegisteringUAs()
{
}

void CUAController::cleanUp()
{
    for(std::map<std::string, CUserAgent*>::iterator it = m_mapOfUserAgents.begin();
        it != m_mapOfUserAgents.end();
        it++)
    {
        delete it->second;
    }

    m_mapOfUserAgents.clear();
}

CUserAgent* CUAController::selectUAS()
{
    CUserAgent* pUA = NULL;
    size_t nIndex = rand()%m_mapOfUASs.size();
    size_t i = 0;
    for(std::map<std::string, CUserAgent*>::iterator it = m_mapOfUASs.begin();
        it != m_mapOfUASs.end();
        ++it)
    {
        if(nIndex == i)
        {
            pUA = it->second;
            break;
        }
        i++;
    }
    return pUA;
}

CUserAgent* CUAController::chooseUAC()
{
    CUserAgent* pUA = NULL;
    size_t nIndex = rand()%m_mapOfUASs.size();
    size_t i = 0;
    for(std::map<std::string, CUserAgent*>::iterator it = m_mapOfUASs.begin();
        it != m_mapOfUASs.end();
        ++it)
    {
        if(nIndex == i)
        {
            pUA = it->second;
            break;
        }
        i++;
    }
    return pUA;
}

void CUAController::makeSimpleCall()
{
    // Choose UAC
//    CUserAgent* pUAC = chooseUAC();

//    // Choose UAS
//    CUserAgent* pUAS = chooseUAS();

//    if(pUAC->getStatus() == CUserAgent::idle
//            || pUAS->getStatus() == CUserAgent::idle)
//    {
//        pUAS->receiveCall();
//        pUAC->initiateCall(pUAS);
//    }
}
