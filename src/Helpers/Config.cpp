/*
 * CConfig.cpp
 *
 *  Created on: Nov 21, 2013
 *      Author: alex
 */

#include "Config.h"
#include <iostream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

//==================================================================================
//
//==================================================================================
CConfig::CConfig() {
    // TODO Auto-generated constructor stub

}

//==================================================================================
// Constructor
//==================================================================================
CConfig::CConfig(const char* chIniFile)
{

    try
    {
        boost::property_tree::ptree pt;
        boost::property_tree::ini_parser::read_ini(std::string(chIniFile), pt);

        if(pt.empty())
        {
            std::cout << "Can't read file " << chIniFile << std::endl;
            return;
        }

        m_nLogLevel = pt.get<int>("General.LogLevel");
        m_strRabbitMqServer = pt.get<std::string>("General.Server");
        m_strRabbitMqName = pt.get<std::string>("General.Name");
        m_strRabbitMqExchange = pt.get<std::string>("General.Exchange");
        m_strRabbitMqExchangeKey = pt.get<std::string>("General.ExchangeKey");
        m_strRabbitMqConsumerTag = pt.get<std::string>("General.ConsumerTag");
        m_strDbLibrary = pt.get<std::string>("General.DbLibrary");

        // CouchDB section
        m_strCouchDbServer = pt.get<std::string>("CouchDB.Server");
        m_strCouchDbBilling = pt.get<std::string>("CouchDB.Billing");
        m_strCouchDbUser = pt.get<std::string>("CouchDB.User");
        m_strCouchDbDevice = pt.get<std::string>("CouchDB.Device");
        m_strCouchDbOrganization = pt.get<std::string>("CouchDB.Organization");
        m_strCouchDbCoin = pt.get<std::string>("CouchDB.Coin");

        // UserAgent Section
        m_nCount = pt.get<int>("UserAgents.Count");
        m_strStartPhoneNumber = pt.get<std::string>("UserAgents.StartPhoneNumber");
        m_strSipServerAddr = pt.get<std::string>("UserAgents.SipServerAddr");
        m_nRegistrationPeriod = pt.get<int>("UserAgents.RegistrationPeriod");

        // Registration test section
        m_nDeviceCount = pt.get<int>("RegistrationTest.Count");
        m_strDeviceDbName = pt.get<std::string>("RegistrationTest.Device");
        m_strDbServerName = pt.get<std::string>("RegistrationTest.Server");
        m_strStatFileName = pt.get<std::string>("RegistrationTest.StatFile");
        m_strRegTestCommand = pt.get<std::string>("RegistrationTest.Command");

        m_strSipServer = pt.get<std::string>("RegistrationTest.SipServer");
        m_strSippXml = pt.get<std::string>("RegistrationTest.SippXml");
        m_strSippCsv = pt.get<std::string>("RegistrationTest.SippCsv");
        m_strLocalIP = pt.get<std::string>("RegistrationTest.LocalIP");
        m_nCountOfCallsPerSec = pt.get<int>("RegistrationTest.CountOfCallsPerSec");
        m_nDeviceOffset = pt.get<int>("RegistrationTest.DeviceOffset");

        char chDirName[256] = {0};
        sprintf(chDirName, "test_%d", getpid());

        char chCommandLine[256] = {0};
        sprintf(chCommandLine,"mkdir %s", chDirName);
        system(chCommandLine);
        sprintf(chCommandLine,"cd %s", chDirName);
        system(chCommandLine);
        sprintf(chCommandLine,"cp %s %s/", m_strSippXml.c_str(), chDirName);
        system(chCommandLine);

        char chFileName[256] =  {0};
        sprintf(chFileName, "%s/%s", chDirName, m_strSippCsv.c_str());
        m_strSippCsv.assign(chFileName);

        sprintf(chFileName, "%s/%s", chDirName, m_strStatFileName.c_str());
        m_strStatFileName.assign(chFileName);

        sprintf(chFileName, "%s/%s", chDirName, m_strSippXml.c_str());
        m_strSippXml.assign(chFileName);

    }
    catch(std::exception& e)
    {
        std::cout << e.what() << std::endl;
    }
}

//==================================================================================
// Destructor
//==================================================================================
CConfig::~CConfig() {
    // TODO Auto-generated destructor stub
}


//==================================================================================
// getLogLevel
//==================================================================================
int CConfig::getLogLevel() const {
    return m_nLogLevel;
}

//==================================================================================
// getRabbitMqExchange
//==================================================================================
const std::string& CConfig::getRabbitMqExchange() const {
    return m_strRabbitMqExchange;
}

//==================================================================================
// getRabbitMqExchangeKey
//==================================================================================
const std::string& CConfig::getRabbitMqExchangeKey() const {
    return m_strRabbitMqExchangeKey;
}

//==================================================================================
// getRabbitMqConsumerTag
//==================================================================================
const std::string& CConfig::getRabbitMqConsumerTag() const {
    return m_strRabbitMqConsumerTag;
}

//==================================================================================
// getRabbitMqName
//==================================================================================
const std::string& CConfig::getRabbitMqName() const {
    return m_strRabbitMqName;
}

//==================================================================================
// getRabbitMqServer
//==================================================================================
const std::string& CConfig::getRabbitMqServer() const {
    return m_strRabbitMqServer;
}

//==================================================================================
// getUACount
//==================================================================================
const int CConfig::getUACount() const
{
    return m_nCount;
}

//==================================================================================
// getUAStartPhoneNumber
//==================================================================================
const std::string CConfig::getUAStartPhoneNumber() const
{
    return m_strStartPhoneNumber;
}

//==================================================================================
// getUASipServerAddr
//==================================================================================
const std::string CConfig::getUASipServerAddr() const
{
    return m_strSipServerAddr;
}

//==================================================================================
// getUARegistrationPeriod
//==================================================================================
const int CConfig::getUARegistrationPeriod() const
{
    return m_nRegistrationPeriod;
}


//==================================================================================
// getDbServerName
//==================================================================================
const std::string CConfig::getDbServerName() const
{
    return m_strDbServerName;
}

//==================================================================================
// getDeviceDbName
//==================================================================================
const std::string CConfig::getDeviceDbName() const
{
    return m_strDeviceDbName;
}

//==================================================================================
// getdeviceCount
//==================================================================================
const int CConfig::getDeviceCount() const
{
    return m_nDeviceCount;
}


//==================================================================================
// getStatisticFileName
//==================================================================================
const std::string CConfig::getStatisticFileName() const
{
    return m_strStatFileName;
}

const std::string CConfig::getRegTestCommand() const
{
    return m_strRegTestCommand;
}


const std::string CConfig::getSipServer() const
{
    return m_strSipServer;
}

const std::string CConfig::getSippXml() const
{
    return m_strSippXml;
}

const std::string CConfig::getSippCsv() const
{
    return m_strSippCsv;
}

const std::string CConfig::getLocalIP() const
{
    return m_strLocalIP;
}

const int CConfig::getCountOfCallsPerSec() const
{
    return m_nCountOfCallsPerSec;
}

const std::string CConfig::getDbLibrary() const
{
    return m_strDbLibrary;
}

const int CConfig::getDeviceOffset() const
{
    return m_nDeviceOffset;
}

//==================================================================================
// getCouchDbServer
//==================================================================================
const std::string& CConfig::getCouchDbServer() const {
    return m_strCouchDbServer;
}

//==================================================================================
// getCouchDbBilling
//==================================================================================
const std::string& CConfig::getCouchDbBilling() const {
    return m_strCouchDbBilling;
}

//==================================================================================
// getCouchDbUser
//==================================================================================
const std::string& CConfig::getCouchDbUser() const {
    return m_strCouchDbUser;
}

//==================================================================================
// getCouchDbDevice
//==================================================================================
const std::string& CConfig::getCouchDbDevice() const {
    return m_strCouchDbDevice;
}

//==================================================================================
// getCouchDbOrganization
//==================================================================================
const std::string& CConfig::getCouchDbOrganization() const {
    return m_strCouchDbOrganization;
}

//==================================================================================
// getCouchDbCoin
//==================================================================================
const std::string& CConfig::getCouchDbCoin() const
{
    return m_strCouchDbCoin;
}
