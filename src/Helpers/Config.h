/*
 * CConfig.h
 *
 *  Created on: Nov 21, 2013
 *      Author: alex
 */

#ifndef CCONFIG_H_
#define CCONFIG_H_

#include <string>

//! \brief Class for reading config file (INI-file). Is implemented as singleton.
class CConfig
{
public:

    //! \brief Destructor
    virtual ~CConfig();

    //! \brief Method that creates and returns singleton object
    static CConfig& getInstance(const char* chIniFileName = 0)
    {
        static CConfig conf(chIniFileName);
        return conf;
    }
    //! \brief Method gets log level
    int getLogLevel() const;

    //! \brief Method gets queue exchnage for rabbitMq
    const std::string& getRabbitMqExchange() const;

    //! \brief Method gets exchange key for rabbitMq
    const std::string& getRabbitMqExchangeKey() const;

    //! \brief Method gets queue name of rabbitMq
    const std::string& getRabbitMqName() const;

    //! \brief Method gets consumer tag of rabbitMq
    const std::string& getRabbitMqConsumerTag() const;

    //! \brief Method gets name of server for rabbitMq
    const std::string& getRabbitMqServer() const;

    const std::string getDbLibrary() const;

    const int getUACount() const;
    const std::string getUAStartPhoneNumber() const;
    const std::string getUASipServerAddr() const;
    const int getUARegistrationPeriod() const;

    const std::string getDeviceDbName() const;
    const std::string getDbServerName() const;
    const int getDeviceCount() const;
    const std::string getStatisticFileName() const;
    const std::string getRegTestCommand() const;
    const std::string getSipServer() const;
    const std::string getSippXml() const;
    const std::string getSippCsv() const;
    const std::string getLocalIP() const;
    const int getCountOfCallsPerSec() const;
    const int getDeviceOffset() const;


    //! \brief Method gets name of server for CouchDB lib
    const std::string& getCouchDbServer() const;

    //! \brief Method gets name of database for billing
    const std::string& getCouchDbBilling() const;

    //! \brief Method gets name of database for user
    const std::string& getCouchDbUser() const;

    //! \brief Method gets name of database for device
    const std::string& getCouchDbDevice() const;

    //! \brief Method gets name of database for organization
    const std::string& getCouchDbOrganization() const;
    const std::string& getCouchDbCoin() const;

    //TODO: Add here other getters for needed variables

private:
    //! \brief Constructor
    CConfig();

    //! \brief Constructor
    //! \param[in] chIniFile Poiner to the path of config file
    CConfig(const char* chIniFile);

    int m_nLogLevel;                        //!< Log level
    std::string m_strRabbitMqServer;        //!< Name of server for rabbitMQ
    std::string m_strRabbitMqName;          //!< Name of queue for rabbitMQ
    std::string m_strRabbitMqExchange;      //!< Name of queue exchange for rabbitMQ
    std::string m_strRabbitMqExchangeKey;   //!< Name of exchange key for rabbitMQ
    std::string m_strRabbitMqConsumerTag;   //!< Name of consumer tag for rabbitMQ

    std::string m_strDbLibrary;

    // UserAgent section
    int m_nCount;
    std::string m_strStartPhoneNumber;
    std::string m_strSipServerAddr;
    int m_nRegistrationPeriod;

    // Registration test section
    int m_nDeviceCount;
    std::string m_strDbServerName;
    std::string m_strDeviceDbName;
    std::string m_strStatFileName;
    std::string m_strRegTestCommand;
    std::string m_strSipServer;
    std::string m_strSippXml;
    std::string m_strSippCsv;
    std::string m_strLocalIP;
    int m_nCountOfCallsPerSec;
    int m_nDeviceOffset;


    std::string m_strCouchDbServer;         //!< Name of server for CouchDb lib
    std::string m_strCouchDbBilling;        //!< Name of database for bililng
    std::string m_strCouchDbUser;           //!< Name of database for user
    std::string m_strCouchDbDevice;         //!< Name of database for device
    std::string m_strCouchDbOrganization;   //!< Name of database for organization
    std::string m_strCouchDbCoin;           //!< Name of database for coin
    //TODO: Add here other needed variables
};

#endif /* CCONFIG_H_ */
