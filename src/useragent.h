#ifndef USERAGENT_H
#define USERAGENT_H

#include <boost/thread.hpp>
#include <boost/thread/condition_variable.hpp>
#include  <string>

class CUAController;
class CUserAgent
{
public:
    typedef enum
    {
        client = 0,
        server
    } typeOfUA;

    typedef enum
    {
        idle = 0 ,
        calling
    }typeOfStatus;

    CUserAgent(std::string strPhoneNumber, CUAController* pUaController);
    virtual ~CUserAgent();
    void start();
    void stop();

    std::string getSipServerAddr()const;
    std::string getPhoneNumber() const;
    int getRegistrationPeriod() const;
    typeOfUA getType() const {return (typeOfUA)m_nType;}
    typeOfUA getStatus() const {return (typeOfUA)m_nStatus;}

    //! \brief  Method for initiating call. It takes xml file
    //!         with name INVINTE_m_strPhoneNumber.xml and runs sipp.
    //!         If this file does not exist Datagenerator will generate it;
    //! \param[in] pUAS User Agents who should receive this call.
    void initiateCall(CUserAgent* pUAS);

    //! \brief  Method for receiving call. It takes xml file
    //!         with name INVINTE_m_strPhoneNumber.xml and runs sipp.
    //!         If this file does not exist Datagenerator will generate it;
    void receiveCall();

private:

    typeOfStatus m_nStatus;
    CUAController* m_pUaController;
    typeOfUA m_nType;
    std::string m_strPhoneNumber;
    int m_nCallDuration;
    int m_nRegistrationPeriod;
    std::string m_strSipServerAddr;

    boost::thread m_RegisteringThread;  //!< Thread that send REGISTER methods
    boost::thread m_CallingThread;      //!< Thread that

    static void registration_proc(void *pArg);
    static void calling_proc(void *pArg);
    boost::mutex m_Lock;
    boost::condition_variable m_Condition;

    bool m_bRun;

    //! \brief  Method for registering phone. It takes xml file
    //!         with name REGISTER_m_strPhoneNumber.xml and runs sipp.
    //!         If this file does not exist Datagenerator will generate it;
    void reg();

};

#endif // USERAGENT_H
