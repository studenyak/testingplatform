#include "useragentdocuments.h"

CUserAgentDocuments::CUserAgentDocuments()
{
}


CUserAgentDocuments::CUserAgentDocuments(const std::string& strPhid,
                    const std::string& strPswd,
                    const int nInternalNumber,
                    const std::string& strUserName,
                    const std::string& strFirstName,
                    const std::string& strUserPswd,
                    const std::string& strCompanyName)
{
    m_Device = CDeviceDocument(strPswd,strPhid);
    m_Extension = CExtensionDocument(nInternalNumber);
    m_User = CUserDocument(strUserPswd, strUserName, strFirstName);
    m_Company = CCompanyDocument(strCompanyName);
}

CUserAgentDocuments::CUserAgentDocuments(const CUserAgentDocuments& src)
{
    m_Device = src.m_Device;
    m_Extension = src.m_Extension;
    m_User = src.m_User;
    m_Company = src.m_Company;
}

CUserAgentDocuments& CUserAgentDocuments::operator=(const CUserAgentDocuments& src)
{
    if(&src != this)
    {
        m_Device = src.m_Device;
        m_Extension = src.m_Extension;
        m_User = src.m_User;
        m_Company = src.m_Company;
    }
    return *this;
}

CDeviceDocument& CUserAgentDocuments::getDevice()
{
    return m_Device;
}

CExtensionDocument& CUserAgentDocuments::getExtension()
{
    return m_Extension;
}

CUserDocument& CUserAgentDocuments::getUser()
{
    return m_User;
}

CCompanyDocument& CUserAgentDocuments::getCompany()
{
    return m_Company;
}

void CUserAgentDocuments::setDevice(const CDeviceDocument &Device)
{
    m_Device = Device;
}

void CUserAgentDocuments::setExtension(const CExtensionDocument &Extension)
{
    m_Extension = Extension;
}

void CUserAgentDocuments::setUser(const CUserDocument &User)
{
    m_User = User;
}

void CUserAgentDocuments::setCompany(const CCompanyDocument &Company)
{
    m_Company = Company;
}
