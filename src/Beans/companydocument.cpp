#include "companydocument.h"

CCompanyDocument::CCompanyDocument(const std::string& strParentId,
                                   const std::string& strShortName)
    : m_strOrganzationType("company")
    , m_strParentId(strParentId)
    , m_strShortName(strShortName)
    , m_strType("organization")
    , m_strTest("")
{
    acceptData();
}

CCompanyDocument::CCompanyDocument(const CCompanyDocument& src)
    : CBaseDocument(src)
{
    m_strOrganzationType = src.m_strOrganzationType;
    m_strParentId = src.m_strParentId;
    m_strShortName = src.m_strShortName;
    m_strType = src.m_strType;
    m_strTest = src.m_strTest;
    acceptData();
}

CCompanyDocument& CCompanyDocument::operator=(const CCompanyDocument& src)
{
    if(&src != this)
    {
        m_strOrganzationType = src.m_strOrganzationType;
        m_strParentId = src.m_strParentId;
        m_strShortName = src.m_strShortName;
        m_strType = src.m_strType;
        m_strTest = src.m_strTest;
        acceptData();
    }
    return *this;
}

CCompanyDocument::~CCompanyDocument()
{
}

void CCompanyDocument::acceptData()
{
    objDocData["organization_type"] = createVariant(m_strOrganzationType);
    objDocData["parent_id"] = createVariant(m_strParentId);
    objDocData["shortname"] = createVariant(m_strShortName);
    objDocData["type"] = createVariant(m_strType);
    objDocData["test"] = createVariant(m_strTest);
    CBaseDocument::acceptData();
}

