#ifndef DEVICEDOCUMENT_H
#define DEVICEDOCUMENT_H
#include "basedocument.h"
class CDeviceDocument : public CBaseDocument
{
public:
    CDeviceDocument(const std::string& strPswd = "",
                    const std::string& strPhid = "",
                    const std::string& strExtId = "",
                    const std::string& strCompanyId = "");

    CDeviceDocument(const CDeviceDocument& src);
    CDeviceDocument& operator=(const CDeviceDocument& src);
    ~CDeviceDocument();

    void setCompanyId(const std::string& strCompId);
    void setExtensionId(const std::string& strExtId);

private:
    void acceptData();

protected:
    std::string m_strPswd;
    std::string m_strType;
    std::string m_strPhid;
    std::string m_strTest;
    std::string m_strExtId;
    std::string m_strCompanyId;
};


#endif // DEVICEDOCUMENT_H
