#include "extensiondocument.h"

CExtensionDocument::CExtensionDocument(const int nNumber,
                                       const std::string& strCompanyId,
                                       const std::string& strUserId)
    : m_strType("extension")
    , m_nInNumber(nNumber)
    , m_strCompanyId(strCompanyId)
    , m_strUserId(strUserId)
    , m_strTest("")
{
    acceptData();
}

CExtensionDocument::CExtensionDocument(const CExtensionDocument& src)
    : CBaseDocument(src)
{
    m_strType = src.m_strType;
    m_nInNumber = src.m_nInNumber;
    m_strCompanyId = src.m_strCompanyId;
    m_strUserId = src.m_strUserId;
    m_strTest = src.m_strTest;

    acceptData();
}

CExtensionDocument& CExtensionDocument::operator =(const CExtensionDocument& src)
{
    if(&src != this)
    {
        CBaseDocument::operator =(src);
        m_strType = src.m_strType;
        m_nInNumber = src.m_nInNumber;
        m_strCompanyId = src.m_strCompanyId;
        m_strUserId = src.m_strUserId;
        m_strTest = src.m_strTest;
        acceptData();
    }
    return *this;
}

CExtensionDocument::~CExtensionDocument()
{
}

void CExtensionDocument::setCompanyId(const std::string &strCompId)
{
    m_strCompanyId = strCompId;
    objDocData["com_id"] = createVariant(m_strCompanyId);
    CBaseDocument::acceptData();
}

void CExtensionDocument::setUserId(const std::string &strUserId)
{
    m_strUserId = strUserId;
    objDocData["user_id"] = createVariant(m_strUserId);
    CBaseDocument::acceptData();
}

void CExtensionDocument::acceptData()
{
    objDocData["type"] = createVariant(m_strType);
    objDocData["in_number"] = createVariant(m_nInNumber);
    objDocData["com_id"] = createVariant(m_strCompanyId);
    objDocData["user_id"] = createVariant(m_strUserId);
    objDocData["test"] = createVariant(m_strTest);

    CBaseDocument::acceptData();
}
