#include "basedocument.h"

CBaseDocument::CBaseDocument()
{
}

CBaseDocument::CBaseDocument(const CBaseDocument& src)
{
    m_strId = src.m_strId;
    m_strRev = src.m_strRev;

    acceptData();
}

CBaseDocument& CBaseDocument::operator=(const CBaseDocument& src)
{
    if(&src != this)
    {
        m_strId = src.m_strId;
        m_strRev = src.m_strRev;
        acceptData();
    }
    return *this;
}

CBaseDocument::~CBaseDocument()
{
}

const std::string& CBaseDocument::getId() const
{
    return m_strId;
}

const boost::any& CBaseDocument::getData() const
{
    return m_anyDocData;
}

void CBaseDocument::setId(const std::string& strId)
{
    m_strId = strId;
    acceptData();
}

void CBaseDocument::acceptData()
{
    objDocData["_id"] = createVariant(m_strId);
    objDocData["rev"] = createVariant(m_strRev);

    m_anyDocData = boost::any_cast<Object>(objDocData);
}
