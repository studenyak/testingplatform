#ifndef COMPANYDOCUMENT_H
#define COMPANYDOCUMENT_H
#include "basedocument.h"

class CCompanyDocument : public CBaseDocument
{
public:
    CCompanyDocument(const std::string& strParentId = "",
                     const std::string& strShortName = "");

    CCompanyDocument(const CCompanyDocument& src);

    CCompanyDocument& operator=(const CCompanyDocument& src);

    ~CCompanyDocument();

private:
    void acceptData();

protected:
    std::string m_strOrganzationType;
    std::string m_strParentId;
    std::string m_strShortName;
    std::string m_strType;
    std::string m_strTest;
};

#endif // COMPANYDOCUMENT_H
