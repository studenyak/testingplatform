#include "userdocument.h"

CUserDocument::CUserDocument(const std::string& strFirstName,
                             const std::string& strOrganizationId,
                             const std::string& strPswd,
                             const std::string& strUserName)
    : m_strFirstName(strFirstName)
    , m_strOrganizationId(strOrganizationId)
    , m_strPswd(strPswd)
    , m_strType("user")
    , m_strUserName(strUserName)
    , m_strTest("")
{
    acceptData();
}

CUserDocument::CUserDocument(const CUserDocument& src)
    : CBaseDocument(src)
{
    m_strFirstName = src.m_strFirstName;
    m_strOrganizationId = src.m_strOrganizationId;
    m_strPswd = src.m_strPswd;
    m_strType = src.m_strType;
    m_strUserName = src.m_strUserName;
    m_strTest = src.m_strTest;
    acceptData();
}

CUserDocument& CUserDocument::operator=(const CUserDocument& src)
{
    if(&src != this)
    {
        CBaseDocument::operator =(src);
        m_strFirstName = src.m_strFirstName;
        m_strOrganizationId = src.m_strOrganizationId;
        m_strPswd = src.m_strPswd;
        m_strType = src.m_strType;
        m_strUserName = src.m_strUserName;
        m_strTest = src.m_strTest;
        acceptData();
    }
    return *this;
}

CUserDocument::~CUserDocument()
{
}

void CUserDocument::setOrganizationId(const std::string &strOrgId)
{
    m_strOrganizationId = strOrgId;
    objDocData["organization_id"] = createVariant(m_strOrganizationId);
    CBaseDocument::acceptData();
}

void CUserDocument::acceptData()
{
    objDocData["first_name"] = createVariant(m_strFirstName);
    objDocData["organization_id"] = createVariant(m_strOrganizationId);
    objDocData["pswd"] = createVariant(m_strPswd);
    objDocData["type"] = createVariant(m_strType);
    objDocData["username"] = createVariant(m_strUserName);
    objDocData["test"] = createVariant(m_strTest);

    CBaseDocument::acceptData();
}
