#ifndef USERAGENTDOCUMENTS_H
#define USERAGENTDOCUMENTS_H

#include "devicedocument.h"
#include "extensiondocument.h"
#include "userdocument.h"
#include "companydocument.h"

class CUserAgentDocuments
{
public:
    CUserAgentDocuments();
    CUserAgentDocuments(const std::string& strPhid,
                        const std::string& strPswd,
                        const int nInternalNumber,
                        const std::string& strUserName,
                        const std::string& strFirstName,
                        const std::string& strUserPswd,
                        const std::string& strCompanyName);
    CUserAgentDocuments(const CUserAgentDocuments& src);
    CUserAgentDocuments& operator=(const CUserAgentDocuments& ua);

    CDeviceDocument&    getDevice();
    CExtensionDocument& getExtension();
    CUserDocument&      getUser();
    CCompanyDocument&   getCompany();

    void setDevice(const CDeviceDocument& Device);
    void setExtension(const CExtensionDocument& Extension);
    void setUser(const CUserDocument& User);
    void setCompany(const CCompanyDocument& Company);

    friend class CUserAgentManager;

private:
    CDeviceDocument     m_Device;
    CExtensionDocument  m_Extension;
    CUserDocument       m_User;
    CCompanyDocument    m_Company;
};

#endif // USERAGENT_H
