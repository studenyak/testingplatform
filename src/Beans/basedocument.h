#ifndef BASEDOCUMENT_H
#define BASEDOCUMENT_H

#include <boost/shared_ptr.hpp>
#include <boost/any.hpp>
#include <deque>
#include <map>
#include <string>
#include <stdio.h>

// some data helpers aligned with TinyJSON implementation
typedef boost::shared_ptr<boost::any>  Variant;
typedef std::deque<Variant>            Array;
typedef std::map<std::string, Variant> Object;

// convenience template
template<typename T>
Variant createVariant(T value){
   return Variant(new boost::any(value));
}

template<>
Variant createVariant<const char*>(const char *value);


class CBaseDocument
{
public:
    CBaseDocument();
    CBaseDocument(const CBaseDocument& src);
    CBaseDocument& operator=(const CBaseDocument& src);
    virtual ~CBaseDocument();
    const boost::any& getData() const;
    const std::string& getId() const;
    void setId(const std::string& strId);

protected:
    virtual void acceptData();

    std::string m_strId;
    std::string m_strRev;
    Object objDocData;
    boost::any m_anyDocData;
};

#endif // BASEDOCUMENT_H
