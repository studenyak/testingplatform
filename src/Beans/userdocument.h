#ifndef USERDOCUMENT_H
#define USERDOCUMENT_H
#include "basedocument.h"

class CUserDocument : public CBaseDocument
{
public:
    CUserDocument(const std::string& strFirstName = "",
                  const std::string& strOrganizationId = "",
                  const std::string& strPswd = "",
                  const std::string& strUserName = "");

    CUserDocument(const CUserDocument& src);
    CUserDocument& operator=(const CUserDocument& src);
    ~CUserDocument();

    void setOrganizationId(const std::string& strOrgId);

private:
    void acceptData();

protected:
    std::string m_strFirstName;
    std::string m_strOrganizationId;
    std::string m_strPswd;
    std::string m_strType;
    std::string m_strUserName;
    std::string m_strTest;
};

#endif // USERDOCUMENT_H
