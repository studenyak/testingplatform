#ifndef EXTENSIONDOCUMENT_H
#define EXTENSIONDOCUMENT_H

#include "basedocument.h"

class CExtensionDocument : public CBaseDocument
{
public:
    CExtensionDocument(const int nNumber = 0,
                       const std::string& strCompanyId = "",
                       const std::string& strUserId = "");
    CExtensionDocument(const CExtensionDocument& src);
    CExtensionDocument& operator=(const CExtensionDocument& src);
    ~CExtensionDocument();

    void setCompanyId(const std::string& strCompId);
    void setUserId(const std::string& strUserId);

private:
    void acceptData();

protected:
    std::string m_strType;
    int         m_nInNumber;
    std::string m_strCompanyId;
    std::string m_strUserId;
    std::string m_strTest;
};

#endif // EXTENSIONDOCUMENT_H
