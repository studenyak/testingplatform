#include "devicedocument.h"

CDeviceDocument::CDeviceDocument(const std::string& strPswd,
                                 const std::string& strPhid,
                                 const std::string& strExtId,
                                 const std::string& strCompanyId)
                    : m_strPswd(strPswd)
                    , m_strType("device")
                    , m_strPhid(strPhid)
                    , m_strTest("")
                    , m_strExtId(strExtId)
                    , m_strCompanyId(strCompanyId)
{
    acceptData();
}

CDeviceDocument::CDeviceDocument(const CDeviceDocument& src)
    : CBaseDocument(src)
{
    m_strPswd = src.m_strPswd;
    m_strType = src.m_strType;
    m_strPhid = src.m_strPhid;
    m_strTest = src.m_strTest;
    m_strExtId = src.m_strExtId;
    m_strCompanyId = src.m_strCompanyId;
    acceptData();
}

CDeviceDocument& CDeviceDocument::operator=(const CDeviceDocument& src)
{
    if(&src != this)
    {
        CBaseDocument::operator =(src);
        m_strPswd = src.m_strPswd;
        m_strType = src.m_strType;
        m_strPhid = src.m_strPhid;
        m_strTest = src.m_strTest;
        m_strExtId = src.m_strExtId;
        m_strCompanyId = src.m_strCompanyId;
        acceptData();
    }

    return *this;
}

CDeviceDocument::~CDeviceDocument()
{
}

void CDeviceDocument::setCompanyId(const std::string &strCompId)
{
    m_strCompanyId = strCompId;
    objDocData["com_id"] = createVariant(m_strCompanyId);
    CBaseDocument::acceptData();
}

void CDeviceDocument::setExtensionId(const std::string &strExtId)
{
    m_strExtId = strExtId;
    objDocData["ext_id"] = createVariant(m_strExtId);
    CBaseDocument::acceptData();
}

void CDeviceDocument::acceptData()
{
    objDocData["pswd"] = createVariant(m_strPswd);
    objDocData["type"] = createVariant(m_strType);
    objDocData["phid"] = createVariant(m_strPhid);
    objDocData["test"] = createVariant(m_strTest);
    objDocData["name"] = createVariant(m_strPhid);
    objDocData["ext_id"] = createVariant(m_strExtId);
    objDocData["com_id"] = createVariant(m_strCompanyId);

    CBaseDocument::acceptData();
}
