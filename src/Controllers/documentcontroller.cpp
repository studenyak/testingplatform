#include "documentcontroller.h"
#include "IDatabase.h"
#include <dlfcn.h>
#include "../Helpers/Config.h"
#include "../RegistrationTest/statisticscollector.h"

//==============================================================================
// CDocumentController
//==============================================================================
CDocumentController::CDocumentController(const std::string& strServerName,
                                         const std::string& strDbName)
    :m_pDatabaseHandle(NULL)
{
    m_pDatabase = connectToDatabase(strServerName, strDbName);
}

//==============================================================================
// ~CDocumentController
//==============================================================================
CDocumentController::~CDocumentController()
{
    // Unloading libCouchDB after all references are clear
    if(m_pDatabase)
    {
        m_pDatabase->release();
        m_pDatabase = 0;
    }
    if(m_pDatabaseHandle)
    {
        dlclose(m_pDatabaseHandle);
        m_pDatabaseHandle = 0;
    }
}

//==============================================================================
// writeToDb()
//==============================================================================
std::string CDocumentController::writeToDb(CBaseDocument* pDoc)
{
    if(!m_pDatabase)
        return "";

    boost::any anyDocData = pDoc->getData();
    CStatisticsCollector* pStat = &CStatisticsCollector::getInstance();
    timeval start_time = pStat->getCurTime();
    std::string strId = m_pDatabase->createDocument(anyDocData);
    timeval end_time = pStat->getCurTime();
    pStat->setCreationTime(pStat->diffTime_msec(start_time, end_time));
    pStat->write();

    return strId;
}

//==============================================================================
// removeFromDb()
//==============================================================================
bool CDocumentController::removeFromDb(std::string& strDocId)
{
    if(!m_pDatabase)
        return false;

    return m_pDatabase->removeDocumentById(strDocId);
}

//==============================================================================
// removeFromDbByParams()
//==============================================================================
bool CDocumentController::removeFromDbByParams(void)
{
    if(!m_pDatabase)
        return false;
    mapOfDataDescriptions dataToGetFromDB;
    dataToGetFromDB["phid"] = DB_STR;
    dataToGetFromDB["type"] = DB_STR;
    dataToGetFromDB["test"] = DB_INT;

    setOfViewConditions viewConditions;
    viewConditions.insert(pairOfOperands("type", "device"));

    setOfDataConditions dataConditions;
    //dataConditions.insert(dataCondition(pairOfOperands("test", ""),GreaterThan));
    mapOfDocuments docs = m_pDatabase->queryDataByParams(dataToGetFromDB,
                                                         viewConditions,
                                                         dataConditions);
    for(mapOfDocuments::iterator it = docs.begin();
        it != docs.end();
        it++)
    {
        m_pDatabase->removeDocumentById(it->first);
    }
    return true;
}

//==============================================================================
// connectToDatabase()
//==============================================================================
IDatabase* CDocumentController::connectToDatabase(const std::string& strServerName,
                                               const std::string& strDatabaseName)
{
    IDatabase* pDatabase = NULL;

    // Load library with database implementation (if it is already not loaded)
    if(!m_pDatabaseHandle)
        m_pDatabaseHandle = dlopen(CConfig::getInstance().getDbLibrary().c_str(), RTLD_LAZY);

    if(!m_pDatabaseHandle)
    {
//        std::cout << "Current dir: " << get_current_dir_name() << std::endl;
        std::cout << dlerror() << std::endl;
        return NULL;
    }

    // Create instance of database object
    typedef IDatabase* (*CREATE_INSTANCE)(const std::string& strServerName,
                                          const std::string& strDatabaseName);

    CREATE_INSTANCE createInstance = (CREATE_INSTANCE) dlsym(m_pDatabaseHandle,
                                                             "createInstance");
    if(!createInstance)
    {
        std::cout << dlerror() << std::endl;
        return NULL;
    }

    CStatisticsCollector* pStat = &CStatisticsCollector::getInstance();
    timeval start_time, end_time;
    start_time = pStat->getCurTime();
    pDatabase = createInstance(strServerName, strDatabaseName);
    end_time = pStat->getCurTime();
    pStat->setConnectionTime(pStat->diffTime_msec(start_time, end_time));
    if(pDatabase && !pDatabase->isConnected())
        pDatabase = NULL;

    return pDatabase;
}
