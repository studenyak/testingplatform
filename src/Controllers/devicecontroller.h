#ifndef DEVICECREATOR_H
#define DEVICECREATOR_H

#include <list>
#include "documentcontroller.h"

class CDeviceController
{
public:
    CDeviceController();
    int createDevices(const unsigned int nDeviceCount);
    int removeDevices(void);

private:
    std::list<std::string> m_listOfDevices;
};

#endif // DEVICECREATOR_H
