#include "../Beans/devicedocument.h"
#include "devicecontroller.h"
#include "../Helpers/Config.h"
#include <fstream>

CDeviceController::CDeviceController()
{
}

int CDeviceController::createDevices(const unsigned int nDeviceCount)
{
    printf("Creating %d devices. Please wait ...\n", nDeviceCount);
    std::ofstream csvRegFile(CConfig::getInstance().getSippCsv().c_str());
    csvRegFile << "SEQUENTIAL" << std::endl;
    unsigned int nEnd = nDeviceCount + (unsigned int)CConfig::getInstance().getDeviceOffset();
    for(unsigned int i = CConfig::getInstance().getDeviceOffset(); i < nEnd; i++)
    {
        // Create document with necessary data
        char buffer [256] = {0};
        sprintf(buffer, "%s%d", "stood", i);
        std::string strPhid(buffer);
        sprintf(buffer, "%s%d", "stoodpswd", i);
        std::string strPswd(buffer);
#ifdef FIRST_START
        CDeviceDocument device(strPswd, strPhid);

        // Write document to database
        CDocumentController controller(CConfig::getInstance().getDbServerName(),
                                       CConfig::getInstance().getDeviceDbName());
        std::string strUserId = controller.writeToDb(&device);
        if(strUserId.empty())
            continue;

        m_listOfDevices.push_back(strUserId);
#endif
        char chBuf[256]={0};
        sprintf(chBuf, "%s;[authentication username=%s password=%s]",strPhid.c_str(),strPhid.c_str(),strPswd.c_str());
        csvRegFile << chBuf << std::endl;
    }
    csvRegFile.close();

#ifdef FIRST_START
    if(nDeviceCount == m_listOfDevices.size() )
        printf("All devices are created.\n");
    else
        printf("Only %ld devices were created.\n",m_listOfDevices.size());
    return m_listOfDevices.size();
#else
    return nDeviceCount;
#endif
}

int CDeviceController::removeDevices(void)
{ 
    printf("Removing devices. Please wait ...\n");
    CDocumentController controller(CConfig::getInstance().getDbServerName(),
                                   CConfig::getInstance().getDeviceDbName());

    int nRemoved = 0;
    for(std::list<std::string>::iterator it = m_listOfDevices.begin();
        it != m_listOfDevices.end();
        it++)
    {
        if(controller.removeFromDb(*it))
            nRemoved++;
    }
    m_listOfDevices.clear();

    printf("Removed %d devices.\n",nRemoved);
    return nRemoved;
}
