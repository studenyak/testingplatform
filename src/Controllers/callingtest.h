#ifndef CALLINGTEST_H
#define CALLINGTEST_H

#include "../Mediators/useragentmanager.h"

class CCallingTest
{
public:
    CCallingTest();
    void start();
    void stop();

private:
    //! \brief Runs sipp to call on ouside number.
    int runOutsideCall(void);
    CUserAgentManager m_UserAgentManager;
    CUserAgentDocuments m_UserAgentDocs;
};

#endif // CALLINGTEST_H
