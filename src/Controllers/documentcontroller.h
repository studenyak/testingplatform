#ifndef COUCHDBDOCUMENTCREATOR_H
#define COUCHDBDOCUMENTCREATOR_H
#include "../Beans/basedocument.h"
#include "../IDatabase.h"

//! \brief Inserts/removes document to database
class CDocumentController
{
public:
    CDocumentController(const std::string &strServerName,
                        const std::string &strDbName);
    ~CDocumentController();

    std::string writeToDb(CBaseDocument* pDoc);

    bool removeFromDb(std::string& strDocId);
    bool removeFromDbByParams();

private:
    IDatabase* connectToDatabase(const std::string& strServerName,
                                 const std::string& strDatabaseName);
    IDatabase* m_pDatabase;
    void* m_pDatabaseHandle;
};

#endif // COUCHDBDOCUMENTCREATOR_H
