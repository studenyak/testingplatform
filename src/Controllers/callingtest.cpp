#include "callingtest.h"
#include "../Helpers/Config.h"

CCallingTest::CCallingTest()
{
}

void CCallingTest::start()
{
    m_UserAgentDocs.setDevice(CDeviceDocument("stood1pswd",
                                              "stood1"));
    m_UserAgentManager.createUserAgent(m_UserAgentDocs);
    runOutsideCall();
}

void CCallingTest::stop()
{
    m_UserAgentManager.removeUserAgent(m_UserAgentDocs);
}

int CCallingTest::runOutsideCall()
{
    printf("Run sipp and wait until it will end testing.\n");
    // Run sipp
//    char chCommandLine[4096]={0};
//    CConfig* pConf = &CConfig::getInstance();
//    sprintf(chCommandLine, "%s %s -sf %s -inf %s -i %s -m %d -l %d -r %d -trace_rtt -trace_screen"
//            , pConf->getRegTestCommand().c_str()
//            , pConf->getSipServer().c_str()
//            , pConf->getSippXml().c_str()
//            , pConf->getSippCsv().c_str()
//            , pConf->getLocalIP().c_str()
//            , nDevices
//            , pConf->getCountOfCallsPerSec()
//            , nDevices);
//    printf("%s\n",chCommandLine);
//    return system(chCommandLine);
    return 0;
}
