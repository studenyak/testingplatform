#include "actor.h"
#include "Config.h"
#include "datagenerator.h"
#include <stdio.h>
#include <time.h>

CActor::CActor(const std::string& strPhid) : m_strPhid(strPhid)
  , m_Thread(CActor::proc, this)
  , m_bRun(true)
{
    CConfig config = CConfig::getInstance("config.ini");

    // Config RabbitMQ
    try
    {
        m_pConnection = Channel::Create(config.getRabbitMqServer());
        m_pConnection->DeclareQueue(config.getRabbitMqName(), false,false,false);
    }
    catch(ChannelException& exception)
    {
        std::cout << "ERROR RabbitMQ: " << exception.what() << std::endl;
    }
    catch(AmqpException& exception)
    {
        std::cout << "ERROR RabbitMQ: " << exception.what() << std::endl;
    }
    catch(std::runtime_error& exception)
    {
        std::cout << "ERROR RabbitMQ: " << exception.what() << std::endl;
    }
}

CActor::~CActor()
{
    stop();
}

void CActor::start()
{
}

void CActor::stop()
{
    m_Condition.notify_one();
}

void CActor::proc(void* pArg)
{
    CActor* pCtx = (CActor*)pArg;
    if(!pCtx)
    {
        printf("ERROR: Context is empty.\n");
        return;
    }

    printf("INFO: Actor %s started working.\n", pCtx->m_strPhid.c_str());
    while(pCtx->m_bRun)
    {
        // Generate new data
        CDataGenerator dg;
        char chMsg[256]={0};
        sprintf(chMsg, "{\"extensionID\":\"%s\", \"callDuration\":%d, \"callDestination\":\"%s\"}",
                pCtx->m_strPhid.c_str(),
                dg.generateCallDuration(),
                dg.generatePhoneNumber().c_str());

        std::string strMsg(chMsg);
        int nRelTime = dg.generateTimeDelay();

        printf("Actor %s has new message:\n\t\t%s\n\t\tthat will be send in %d seconds.\n", pCtx->m_strPhid.c_str(), strMsg.c_str(), nRelTime);
        printf("====================================================================\n");

        // Wait for your time
        boost::system_time const timeout = boost::get_system_time() + boost::posix_time::seconds(nRelTime);
        boost::unique_lock<boost::mutex> lock(pCtx->m_Lock);
        if(pCtx->m_Condition.timed_wait(lock,timeout) == false)
        {
            // Send message to message queue

            timeval time;
            gettimeofday(&time, NULL);
            char bufTime[256] = {0};
            struct tm * timeinfo;
            timeinfo = localtime (&time.tv_sec);
            strftime(bufTime,sizeof(bufTime),"%b %e %T",timeinfo);

            printf("Actor %s sends message on %s\n", pCtx->m_strPhid.c_str(), bufTime);
            pCtx->sendMsg(strMsg);
        }
        else
            pCtx->m_bRun = false;
    }
    printf("Actor %s stops working.\n", pCtx->m_strPhid.c_str());
}

void CActor::sendMsg(const std::string& strMsg)
{
    // Push message into queue
    BasicMessage::ptr_t pInMsg = BasicMessage::Create();
    pInMsg->Body(strMsg);
    m_pConnection->BasicPublish(CConfig::getInstance().getRabbitMqExchange(),
                              CConfig::getInstance().getRabbitMqExchangeKey(),
                              pInMsg);
}
