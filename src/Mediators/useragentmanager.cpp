#include "useragentmanager.h"
#include "../Helpers/Config.h"

CUserAgentManager::CUserAgentManager()
{
}

void CUserAgentManager::createUserAgent(CUserAgentDocuments &Docs)
{
    CConfig config = CConfig::getInstance();
    // Write company document to database
    m_pDocumentManager->createDocument(config.getCouchDbOrganization(),
                                       Docs.m_Company);

    // Write User document to database
    Docs.m_User.setOrganizationId(Docs.m_Company.getId());
    m_pDocumentManager->createDocument(config.getCouchDbUser(),
                                       Docs.m_User);

    // Write extension document to database
    Docs.m_Extension.setUserId(Docs.m_User.getId());
    Docs.m_Extension.setCompanyId(Docs.m_Company.getId());
    m_pDocumentManager->createDocument(config.getCouchDbUser(),
                                       Docs.m_Extension);

    // Write device document to database
    Docs.m_Device.setExtensionId(Docs.m_Extension.getId());
    Docs.m_Device.setCompanyId(Docs.m_Company.getId());
    m_pDocumentManager->createDocument(config.getCouchDbDevice(),
                                       Docs.m_Device);
}

void CUserAgentManager::removeUserAgent(CUserAgentDocuments &Docs)
{
    CConfig config = CConfig::getInstance();
    // Remove related documents from database
    m_pDocumentManager->removeDocument(config.getCouchDbOrganization(),
                                       Docs.m_Company);
    m_pDocumentManager->removeDocument(config.getCouchDbUser(),
                                       Docs.m_Extension);
    m_pDocumentManager->removeDocument(config.getCouchDbDevice(),
                                       Docs.m_Device);
    m_pDocumentManager->removeDocument(config.getCouchDbUser(),
                                       Docs.m_User);

    // Update ids in related documents
    Docs.m_User.setOrganizationId(Docs.m_Company.getId());
    Docs.m_Extension.setUserId(Docs.m_User.getId());
    Docs.m_Extension.setCompanyId(Docs.m_Company.getId());
    Docs.m_Device.setExtensionId(Docs.m_Extension.getId());
    Docs.m_Device.setCompanyId(Docs.m_Company.getId());
}
