/*
 * ConnectionPool.cpp
 *
 *  Created on: Oct 4, 2013
 *      Author: alex
 */

#include "ConnectionPool.h"
#include <dlfcn.h>
#include "../Helpers/Config.h"
#include <stdio.h>

CConnectionPool::CConnectionPool() : m_pDatabaseHandle(0)
{
	// TODO Auto-generated constructor stub

}

CConnectionPool::~CConnectionPool()
{
	// Unloading libCouchDB after all references are clear
	if(m_pDatabaseHandle)
	{
		dlclose(m_pDatabaseHandle);
		m_pDatabaseHandle = 0;
	}
}

void CConnectionPool::add(const std::string& strDbNamae, IDatabase* pDb)
{
	boost::mutex::scoped_lock(m_Lock);
	m_mapOfConnections[strDbNamae] = pDb;
}

void CConnectionPool::remove(const std::string& strDbName)
{
	boost::mutex::scoped_lock(m_Lock);
	std::map<std::string, IDatabase*>::iterator it = m_mapOfConnections.find(strDbName);
	if(it != m_mapOfConnections.end())
		m_mapOfConnections.erase(strDbName);
}

IDatabase* CConnectionPool::get(const std::string& strDbName)
{
	boost::mutex::scoped_lock(m_Lock);
	std::map<std::string, IDatabase*>::iterator it = m_mapOfConnections.find(strDbName);
	if(it == m_mapOfConnections.end())
	{
        IDatabase* pDb = connectToDatabase(CConfig::getInstance().getCouchDbServer(), strDbName);
		if(!pDb)
		{
            printf("ERROR: Can't connect to database %s", strDbName.c_str());
			return 0;
		}

		m_mapOfConnections[strDbName] = pDb;
	}

	return m_mapOfConnections[strDbName];
}

//==============================================================================
// connectToDatabase()
//==============================================================================
IDatabase* CConnectionPool::connectToDatabase(const std::string& strServerName,
											 const std::string& strDatabaseName)
{
    printf("INFO: Connect to database %s/%s", strServerName.c_str(), strDatabaseName.c_str());
	IDatabase* pDatabase = NULL;

	// Load library with database implementation (if it is already not loaded)
	if(!m_pDatabaseHandle)
        m_pDatabaseHandle = dlopen(CConfig::getInstance().getDbLibrary().c_str(), RTLD_LAZY);

	if(!m_pDatabaseHandle)
	{
//		std::cout << "Current dir: " << get_current_dir_name() << std::endl;
		std::cout << dlerror() << std::endl;
		return NULL;
	}

	// Create instance of database object
	typedef IDatabase* (*CREATE_INSTANCE)(const std::string& strServerName,
										  const std::string& strDatabaseName);

	CREATE_INSTANCE createInstance = (CREATE_INSTANCE) dlsym(m_pDatabaseHandle,
															 "createInstance");
	if(!createInstance)
	{
		std::cout << dlerror() << std::endl;
		return NULL;
	}

	pDatabase = createInstance(strServerName, strDatabaseName);
	if(pDatabase && !pDatabase->isConnected())
		pDatabase = NULL;

	return pDatabase;
}

