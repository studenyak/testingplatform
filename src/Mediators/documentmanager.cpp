#include "documentmanager.h"
#include "ConnectionPool.h"

CDocumentManager::CDocumentManager()
{
}

CDocumentManager::~CDocumentManager()
{
}

std::string CDocumentManager::createDocument(const std::string& strDbName, CBaseDocument& doc)
{
    IDatabase* pDatabase = CConnectionPool::getInstance().get(strDbName);
    if(!pDatabase)
        return "";

    boost::any anyDocData = doc.getData();
    std::string strId = pDatabase->createDocument(anyDocData);
    doc.setId(strId);

    return strId;
}


bool CDocumentManager::removeDocument(const std::string &strDbName, CBaseDocument& doc)
{
    IDatabase* pDatabase = CConnectionPool::getInstance().get(strDbName);

    if(!pDatabase || doc.getId().empty())
        return false;

    if(!pDatabase->removeDocumentById(doc.getId()))
        return false;

    // Reset id of deleted document to show that it is not more in database
    doc.setId("");

    return true;
}
