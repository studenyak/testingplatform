/*
 * ConnectionPool.h
 *
 *  Created on: Oct 4, 2013
 *      Author: alex
 */

#ifndef CONNECTIONPOOL_H_
#define CONNECTIONPOOL_H_
#include <map>
#include <string>
#include <boost/thread/mutex.hpp>
#include "../IDatabase.h"

// Singleton
class CConnectionPool
{
public:
	static CConnectionPool& getInstance()
	{
		static CConnectionPool instance;
		return instance;
	}

	virtual ~CConnectionPool();

	void add(const std::string& strDbName, IDatabase* pDb);
	void remove(const std::string& strDbName);
	IDatabase* get(const std::string& strDbName);

private:
	CConnectionPool();
	CConnectionPool( const CConnectionPool&);
	CConnectionPool& operator=( CConnectionPool& );


    void*       m_pDatabaseHandle;  //!< Handle of loaded database library
                                    //!<

	std::map<std::string, IDatabase*> m_mapOfConnections;
	boost::mutex m_Lock;

	//! \brief Connects to database.
	//!
	//! Loads responsible library ( LIB_DATABASE_PATH ) for	working with database
	//! and connects to it.
	//! \param[in] strServerName Server IP where the database is.
	//! \param[in] strDatabaseName Name of database to connect
	//! \return Interface to the connected database or NULL if connection was failed.
	IDatabase* connectToDatabase(const std::string& strServerName,
	                             const std::string& strDatabaseName);
};

#endif /* CONNECTIONPOOL_H_ */
