#ifndef USERAGENTMANAGER_H
#define USERAGENTMANAGER_H

#include "../Beans/useragentdocuments.h"
#include "documentmanager.h"

class CUserAgentManager
{
public:
    CUserAgentManager();

    //! \brief Creates set of needed documents for calling
    //!          and writes this set into database
    void createUserAgent(CUserAgentDocuments& UaDocs);

    //! \brief Removes documents created for user agent
    void removeUserAgent(CUserAgentDocuments& UaDocs);

private:
    CDocumentManager* m_pDocumentManager;
};

#endif // USERAGENTMANAGER_H
