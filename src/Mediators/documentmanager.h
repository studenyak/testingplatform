#ifndef DOCUMENTMANAGER_H
#define DOCUMENTMANAGER_H

#include "../Beans/basedocument.h"
#include "ConnectionPool.h"

class CDocumentManager
{
public:
    CDocumentManager();
    ~CDocumentManager();

    //! \brief Creates document in database
    //! \return id of new created document
    std::string createDocument(const std::string& strDbName, CBaseDocument& doc);

    //! \brief Rmoves document from database
    //! \param doc Document to be deleted
    //! \return true if operation was successful.
    bool removeDocument(const std::string& strDbName, CBaseDocument &doc);

};

#endif // DOCUMENTMANAGER_H
