<?xml version="1.0" encoding="ISO-8859-2" ?>

<!--  Use with CSV file struct like: 3000;192.168.1.106;[authentication username=3000 password=3000];
(user part of uri, server address, auth tag in each line)
-->

<scenario name="register_client">
		<nop>
			<action>
			<assignstr assign_to="ua" value="%s"/>
			<assignstr assign_to="regPeriod" value="%d"/>
			</action>
		</nop>

		<send retrans="500">
			<![CDATA[

			REGISTER sip:[remote_ip] SIP/2.0
			Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
			From: <sip:[$ua]@[remote_ip]>;tag=[call_number]
			To: <sip:[$ua]@[remote_ip]>
			Call-ID: [call_id]
			CSeq: [cseq] REGISTER
			Contact: sip:[$ua]@[local_ip]:[local_port]
			Max-Forwards: 10
			Expires: [$regPeriod]
			User-Agent: SIPp/Win32
			Content-Length: 0

			]]>
		</send>

		<!-- kamailio -->
		<recv response="100" optional="true"></recv>
		<recv response="200" optional="true"></recv>

	<!-- response time repartition table (ms)   -->
	<ResponseTimeRepartition value="10, 20, 30, 40, 50, 100, 150, 200"/>

	<!-- call length repartition table (ms)     -->
	<CallLengthRepartition value="10, 50, 100, 500, 1000, 5000, 10000"/>

</scenario>
